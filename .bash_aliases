alias mng='python manage.py'
alias rs='python manage.py runserver'
alias curl_json='curl -H "Accept: application/json; indent=4"'
alias aliases="code --wait ~/.bash_aliases && source ~/.bash_aliases"
alias clean_pyc="find . -name \*.pyc -delete"

# Include custom git prompt
. "$HOME/.git_prompt"
