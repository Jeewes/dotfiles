wo() {
    cd ~/projects/$1
    . ./venv/bin/activate
    if [ -d $1 ]; then
        cd $1
        return
    fi
    local appRepo="$( echo -e "$1" | tr '-' '_')"
    if [ -d $appRepo ]; then
        cd $appRepo
        return
    fi
    if [ -d project ]; then
        cd project
        return
    fi
    if [ -d app ]; then
        cd app
        return
    fi
}

wod() {
    wo $1
    docker-compose up
}

_wo()
{
    local cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "$(for i in $(ls -d1 ~/projects/*/); do basename $i; done)" -- $cur) )
}

complete -F _wo wo
complete -F _wo wod

export -f wo
export -f wod
